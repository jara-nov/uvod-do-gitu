﻿using System;
using System.Text;

namespace FIS
{
    using Extension;
    class Program
    {
        static void Main(string[] args)
        {
            //string z konzole do CamelCase
            string input = Console.ReadLine();
            Console.WriteLine(input.ToCamelCase());
        }
    }
}


namespace Extension
{
    public static class StringRozsireni
    {
        public static string ToCamelCase(this String str)
        {
            StringBuilder sb = new StringBuilder();
            string[] words = str.Split(new char[] { ' ', '.', ',','?','!' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string word in words)
            {
                sb.Append(char.ToUpper(word[0]));
                for (int i = 1; i < word.Length; i++)
                    sb.Append(char.ToLower(word[i]));
            }
            return sb.ToString();
        }
    }
}